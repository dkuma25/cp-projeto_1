# Perceptron

## O que é Perceptron

O perceptron  é o tipo mais básico de rede neural. O primeiro modelo de uma rede neural artificial demonstrou matematicamente a possibilidade de simulação do aprendizado cerebral.
Este modelo simulava um neurônio com entradas e pesos que, ajustados teriam o "poder" de aprender a se comportar de determinada forma.

## Perceptron Paralelo

Paralelização de um algoritmo Perceptron para a disciplina de Computação Paralela.

## Instruções
Para criar um arquivo de entrada diferente execute o comando abaixo substituindo o `num_entradas` pelo valor desejado.
```console
. create_arq.sh num_entradas
```

Para executar o código, execute o comando:
```console
. compile_run.sh
```

Para executar ambos os comandos acima, execute o comando abaixo substituindo o `num_entradas` pelo valor desejado.
```console
. full_routine.sh num_entradas
```