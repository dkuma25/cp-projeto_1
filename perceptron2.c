#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <string.h>

#define REPETICOES 500

double g(double in){
    return (1/ (1+ pow(2.718281828, in* -1)));
}

double devG(double in){
    return g(in) * (1-g(in));
}

int main(){
    int lin;
    int col=3;
    FILE *file;
    file = fopen("in.in","rt");
    fscanf(file, "%d", &lin);
    double m[lin][col];
    int a,b;
	double maior=0;
	double menor = 1000;
    for(a=0;a<lin;a++){
            for(b=0;b<col;b++){
                    fscanf(file, "%lf", &m[a][b]);
					if(b<col-1){
						if(m[a][b]>maior) maior = m[a][b];
						else if(m[a][b]<menor) menor = m[a][b];
					}
            }
    }
	for(a=0;a<lin;a++){
		for(b=0;b<col-1;b++){
			m[a][b] = (m[a][b] - menor)/ (maior-menor); 
		}
	}
    
    fclose(file);
    
    double entrada;
    double dadoLimite = 0.3;
    double alpha = 0.5;
    double erro;
    
    for(int v=1;v<=REPETICOES;v++){
        double w[col-1];
        for(int i=0;i<col-1;i++){
            if(i<=2) w[i] = i*0.5;
            else w[i] = (i/10)-(int)(i/10);
        }
        
        int numEpocas = 0;
        do{
            int i;
            for(i=0;i<lin;i++){
                double entrada = 0;
                int j;
                for(int j=0;j<col-2;j++){
                    entrada = entrada + (w[j]*m[i][j]);
                }
                erro = m[i][col-1] - g(entrada);
                for(int j=0;j<col-2;j++){
                    w[j] = w[j]+(alpha*devG(entrada)*m[i][j]);
                }    
            }
            numEpocas++;
        }while(numEpocas < 1000);
    }
    printf("Sequencial\n");

    return 0;
}