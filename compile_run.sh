#!/bin/bash

# Just compile and run perceptron code

gcc perceptron2.c -o perceptron2 -lm
gcc perceptron2_parallel.c -o perceptron2Parallel -lm -fopenmp

time ./perceptron2
time ./perceptron2Parallel 1
time ./perceptron2Parallel 2
time ./perceptron2Parallel 4
time ./perceptron2Parallel 8